#!/usr/bin/python3
# -*- encoding: utf-8 -*-

with open('VERSION', 'r') as ofd:
    version = ofd.read().strip()

with open('README.md', 'r') as ofd:
    readme = ofd.read()

from setuptools import setup, find_namespace_packages
setup(
    name='featuring',
    version=version,
    url='https://gitlab.com/drachlyznardh/featuring',
    author='Ivan Simonini',
    author_email='ivan.simonini@roundhousecode.com',
    project_urls={
        'Source':'https://gitlab.com/drachlyznardh/featuring',
        'Tracker':'https://gitlab.com/drachlyznardh/featuring/issues'
    },
    description='A Python setup-time configuration manager for Python',
    long_description=readme,
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console'
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6'
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    package_dir={'':'src'},
    packages=find_namespace_packages(where='src'),
    scripts=['scripts/featuring'],
    include_package_data=True
)

