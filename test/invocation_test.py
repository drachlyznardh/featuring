#!/usr/bin/python3
# -*- encoding: utf-8 -*-

import pytest, featuring, pkg_resources

with open(pkg_resources.resource_filename('featuring', 'VERSION'), 'r') as ofd:
    _version = ofd.read().strip()

with open(pkg_resources.resource_filename('featuring', 'LICENSE'), 'r') as ofd:
    _license = ofd.read().strip()

def test_version(capsys):
    with pytest.raises(SystemExit) as e:
        featuring.setup('--version'.split())
    assert e.type == SystemExit
    assert e.value.code == 0
    out, err = capsys.readouterr()
    assert _version in out
    assert err == ''

def test_license(capsys):
    with pytest.raises(SystemExit) as e:
        featuring.setup('--license'.split())
    assert e.type == SystemExit
    assert e.value.code == 0
    out, err = capsys.readouterr()
    assert _license == out.strip()
    assert err == ''

def test_show_version_license(capsys):
    with pytest.raises(SystemExit) as e:
        featuring.setup('--show-version --license'.split())
    assert e.type == SystemExit
    assert e.value.code == 0
    out, err = capsys.readouterr()
    assert _version in out
    assert _license in out
    assert err == ''

def test_version_show_license(capsys):
    with pytest.raises(SystemExit) as e:
        featuring.setup('--version --show-license'.split())
    assert e.type == SystemExit
    assert e.value.code == 0
    out, err = capsys.readouterr()
    assert _version in out
    assert _license not in out
    assert err == ''

def test_show_version_show_license(capsys):
    with pytest.raises(featuring.EmptyListError) as e:
        featuring.setup('--show-version --show-license'.split())
    assert e.type == featuring.EmptyListError
    out, err = capsys.readouterr()
    assert _version in out
    assert _license in out
    assert err == ''

