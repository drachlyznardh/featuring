#!/usr/bin/python3
# -*- encoding: utf-8 -*-

import pytest, featuring, pkg_resources, os, time, shutil

def _name(name, value): return 'feature["{}"] = {}'.format(name, value)
def _enable(name): return _name(name, True)
def _disable(name): return _name(name, False)

def test_create_00(capsys, tmpdir):
    os.chdir(tmpdir)
    featuring.setup('--list FTR_A --enable FTR_A --filename create_00.pyc'.split())
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''
    time.sleep(.01)
    import create_00 as c
    assert c.get('FTR_A')
    shutil.rmtree(tmpdir)

def test_create_01(capsys, tmpdir):
    os.chdir(tmpdir)
    featuring.setup('--list FTR_A --disable FTR_A --filename create_01.pyc'.split())
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''
    time.sleep(.01)
    import create_01 as c
    assert not c.get('FTR_A')
    shutil.rmtree(tmpdir)

