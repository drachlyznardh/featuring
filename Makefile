
TARGETDIR=featuring-$(shell cat VERSION )
TARFILE=dist/$(TARGETDIR).tar.gz

all: test

test:
	@python3 -m setup pytest

fulltest: targetdir_clean package
	tar xvzf $(TARFILE) && cd $(TARGETDIR) && $(MAKE) test

package:
	@python3 setup.py sdist

install: package
	@pip3 install $(TARFILE)

clean: targetdir_clean
	@$(RM) -rf dist/ build/ .eggs/ featuring.egg-info/
	@$(shell find . -type d -name __pycache__ | xargs $(RM) -rf )
	@$(shell find . -type f -name *.pyc -delete )

targetdir_clean:
	@$(RM) -rf $(TARGETDIR)

.PHONY: run test fulltest
.PHONY: package install
.PHONY: clean targetdir_clean

