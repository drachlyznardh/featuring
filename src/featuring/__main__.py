#!/usr/bin/python3
# -*- encoding: utf-8 -*-

import pkg_resources

_true_alternatives  = ('true', 't', 'yes', 'y')
_false_alternatives = ('false', 'f', 'no', 'n')

class EmptyListError(Exception):
    def __init__(self):
        self.message = 'Feature list is empty'

class DoubleDefinition(Exception):
    def __init__(self, truth, name):
        self.message = 'Feature {} is listed twice, both enabled and disabled'.format(name)

def _composeLists(full, value, enable, disable, ask):
    if len(full.union(enable, disable, ask)) < 1: raise EmptyListError()
    _dict = { e: value for e in full}
    for e in enable:
        if e in disable: raise DoubleDefinition(e)
        _dict[e] = True
    for e in disable: _dict[e] = False
    return _dict

def _stringToPyc(source, destination):
    code = compile(source, '<string>', 'exec')
    import importlib
    bytecode = importlib._bootstrap_external._code_to_bytecode(code, 0)
    importlib._bootstrap_external._write_atomic(destination, bytecode, 0o666)

def _composeVerificationFunction(dump):
    return 'def {n}(c):{nl}{t}for k,v in {d}.items():{nl}{t}{t}if c.get(k) != v:raise ValueError(\'{m}\')'.format(
        n='verificationFunction', d=dump, m='Configuration cannot be trusted', nl='\n', t='    ')

def _dumpVerificationFunction(dump, destination):
    with open(destination, 'w') as ofd:
        print(_composeVerificationFunction(dump), file=ofd)

def _confirm(_dict, ask):
    def _2bool(name):
        while True:
            v = input('Enable {}? [t]rue/[y]es [f]alse/[n]o '.format(e)).lower()
            if v in  _true_alternatives: return True
            if v in _false_alternatives: return False

    for e in ask: _dict[e] = _2bool(e)
    _dumpVerificationFunction(_dict, 'featuring_verification.py')
    return _dict

def _sanitize(name):
    if len(name) > 4 and name[-4:] == '.pyc': return name
    return '{}.pyc'.format(name)

def _create(_dict, ask, filename):

    dump = ',\n'.join(['"{}": {}'.format(k,v) for k, v in _confirm(_dict, ask).items()])
    _stringToPyc('''def get(name):
        return {{
            {}
        }}.get(name)'''.format(dump), _sanitize(filename))

def _verify(_dict, ask, filename):

    print('Verifying…')
    from os import path
    targetName = path.basename(path.splitext(filename)[0])
    import importlib
    spec = importlib.util.spec_from_file_location(targetName, filename)
    targetModule = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(targetModule)
    for e in ask:
        eValue = _dict.get(e)
        fValue = targetModule.get(e)
        print('\tExpecting {} for {} got {}'.format(eValue, e, fValue))
        if eValue != fValue: raise SystemExit(1)
    print('Verified')
    raise SystemExit(1)

_action = { 'create': _create, 'verify': _verify }

with open(pkg_resources.resource_filename(__name__, 'VERSION'), 'r') as ofd:
    _version = ofd.read().strip()

with open(pkg_resources.resource_filename(__name__, 'LICENSE'), 'r') as ofd:
    _license = ofd.read().strip()

def _parseArgs(args):

    import argparse

    def _2bool(inV):
        v = inV.lower()
        if v in  _true_alternatives: return True
        if v in _false_alternatives: return False
        raise argparse.ArgumentTypeError('Please specify either [t]rue or [f]alse')

    p = argparse.ArgumentParser(
        description='Manage features from comand line')

    vgroup = p.add_mutually_exclusive_group()
    vgroup.add_argument('--version',      dest='version', action='store_const',
        const=1, default=0, help='Print version and exit')
    vgroup.add_argument('--show-version', dest='version', action='store_const',
        const=2, default=0, help='Print version and continue')

    lgroup = p.add_mutually_exclusive_group()
    lgroup.add_argument('--license',      dest='license', action='store_const',
        const=1, default=0, help='Print license and exit')
    lgroup.add_argument('--show-license', dest='license', action='store_const',
        const=2, default=0, help='Print license and continue')

    dgroup = p.add_mutually_exclusive_group()
    dgroup.add_argument('--true',  dest='value', action='store_true',
        help='Set default value to true')
    dgroup.add_argument('--false', dest='value', action='store_false',
        help='Set default value to false')
    dgroup.add_argument('--value', dest='value', action='store', type=_2bool, default=False,
        help='Set default value')

    p.add_argument('a', metavar='Action', choices=_action.keys(),
        nargs='?', default='create', help='either {}'.format(' or '.join(_action.keys())))

    p.add_argument('-l', '--list',    nargs='+', metavar='FEATURE', default=[], help='List of undefined features')
    p.add_argument('-a', '--ask',     nargs='+', metavar='FEATURE', default=[], help='List of features to be queried')
    p.add_argument('-e', '--enable',  nargs='+', metavar='FEATURE', default=[], help='List of enabled features')
    p.add_argument('-d', '--disable', nargs='+', metavar='FEATURE', default=[], help='List of disabled features')

    p.add_argument('-f', '--filename', metavar='FILE', default='featuring_conf.pyc', help='Output module filename')

    return p.parse_args(args)

def setup(inArgs):

    args = _parseArgs(inArgs)
    if args.version > 0:
        print('Featuring v{version}'.format(version=_version))
        if args.version < 2: raise SystemExit(0)
    if args.license > 0:
        print('{license}'.format(license=_license))
        if args.license < 2: raise SystemExit(0)

    _dict = _composeLists(frozenset(args.list), args.value, frozenset(args.enable), frozenset(args.disable), frozenset(args.ask))
    _action.get(args.a)(_dict, args.ask, args.filename)

if __name__ == '__main__':
    import sys
    setup(sys.argv[1:])

