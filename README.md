# Featuring

_Featuring_ is a tool to generate and manage static configuration objects for
Python applications, thus allowing an installer to statically select which
feature may be enable or disabled on a specific installation.

